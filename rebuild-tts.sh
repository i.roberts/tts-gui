#!/bin/bash
env=prod
imageName=tts-demo:latest
containerName=tts-demo

if [ $env = 'dev' ]
then
  echo -e "\e[1;30mBuilding for\e[0m \e[31mdev\e[0m \e[1;30menvironment\e[0m"
  echo -e "\e[1;30mStopping old container...\e[0m"
  docker stop $containerName

  echo -e "\e[1;30mBuilding image...\e[0m"
  docker build --rm -f "Dockerfile" -t $imageName .

  echo -e "\e[1;30mRun new container and map it to localhost:8080 ...\e[0m"
  docker run -d --rm -p 8080:80 --name $containerName $imageName
else
  echo -e '\e[31m---------------------------------------------\e[0m'
  echo -e '\e[31m Bilding for production and pushing remotely \e[0m'
  echo -e '\e[31m---------------------------------------------\e[0m'
  echo 'Stopping old container...'
  docker stop $containerName

  echo 'Building image...'
  docker build --rm -f "Dockerfile" -t $imageName .  

  echo Run new container...
  docker run -d -p 7667:80 $imageName

  echo 'Login to remote server'
  docker login elgrid.azurecr.io

  echo 'Tag image for pushing...'
  docker tag $containerName elgrid.azurecr.io/tts-demo

  echo 'Push image to remote server...'
  docker push elgrid.azurecr.io/tts-demo
fi